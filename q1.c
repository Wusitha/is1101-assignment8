#include <stdio.h>
#include <string.h>

struct Student {
	char fname[40];
	char subject[30];
	int marks;
};

void display(struct Student std);

int main(){

	int limit;
	char name[40];
	char sbj[30];
	int mark;

	while(limit < 5){
		printf("Enter number of students: ");
		scanf("%d",&limit);

		if(limit < 5){
			printf("Number of students should be more than 5.\nPlease enter again.");
		}
	}

	struct Student std[limit];

	for(int i = 0; i < limit; i++){
		printf("\nStudent%d\n", i+1);
		printf("Enter student first name: ");
		scanf("%s", &name);

		printf("Enter subject name: ");
		scanf("%s", &sbj);

		printf("Enter marks: ");
		scanf("%d", &mark);

		strcpy(std[i].fname, name);
		strcpy(std[i].subject,sbj);
		std[i].marks = mark;
	}

	for(int i = 0; i < limit; i++){
		display(std[i]);
	}

	return 0;

}

void display(struct Student std){
	
	printf("Name: %s\n", std.fname);
	printf("Subject: %s\n", std.subject);
	printf("Marks: %d\n\n", std.marks);
}
